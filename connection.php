<?php  
	$jsonraw = file_get_contents("SETUP/conf.json");
	$conf_array = json_decode($jsonraw,true);
	$mysqli = new mysqli($conf_array['server'], $conf_array['user'], $conf_array['pwd'], $conf_array['db']);
	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
		die("Fatal error occured..");
	}
?>
