<?php
// log complete request data to file api.log
//error_log($_SERVER['REQUEST_METHOD']." ::: ".var_export($_REQUEST,true));   //,3,"api.log");

// include php snippet holding connect data. initiates connection, instantiates connection object "$mysqli"
include("connection.php");

// IF we got a POST-Request:
if($_SERVER['REQUEST_METHOD']=="POST"){
	// check if all required parameters are present and of correct type
	$req = json_decode(file_get_contents('php://input'),true);
	//echo(var_export($req,true));
	if(isset($req['uid']) && isset($req['q1']) && isset($req['q2']) && isset($req['q3']) && isset($req['q4']) && isset($req['q5']) && isset($req['q6']) && isset($req['use_bot'])){

		if (!$insert_stmt = $mysqli->prepare("insert into surveys values(?,current_timestamp(),?,?,?,?,?,?,?);")){
			echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
		}
		else{
			$insert_stmt->bind_param("siiiiiii",$req['uid'],$req['q1'],$req['q2'],$req['q3'],$req['q4'],$req['q5'],$req['q6'],$req['use_bot']);
			$insert_stmt->execute();
			$insert_stmt->close();
			echo "True";
		}


	}
	else{
		echo('Received a mal-formed POST request:\nRequired params: uid (string), q1 (0,1) q2-q6 (1..7), use_bot (0,1)');
		print_r($req);
	}
}


// ...IF we got a GET-Request:
elseif($_SERVER['REQUEST_METHOD']=="GET" && isset($_GET['uid'])){
		$select_stmt = $mysqli->prepare("select * from surveys where userID = ? order by timestamp desc;");    
		$select_stmt->bind_param("s",$_REQUEST['uid']);
		$select_stmt->execute();
		$result = $select_stmt->get_result();
		echo(var_export($result->fetch_assoc(),true));
		$select_stmt->close();
	}


// ..ELSE (unknown Method or GET without uid) ==> return sample rows (default)
else{
	echo fetch_surveys_sample();
	
}


function fetch_surveys_sample(){
	include("connection.php");
	$results = $mysqli->query("select * from surveys order by timestamp desc limit 100;");
		// build static html (doc header, beginning of body & table)
		$htmlstring = '<html>    
				<body>    
					<h2>Latest 100 survey-records:</h2>
					<table width = "50%" border = "1" cellspacing = "1" cellpadding = "1">    
						<tr class="header">    
							<td>Id</td>    
							<td>Date, Time</td>    
							<td>Answer Q1</td>    
							<td>Answer Q2</td>    
							<td>Answer Q3</td>    
							<td>Answer Q4</td>    
							<td>Answer Q5</td>    
							<td>Answer Q6</td>    
							<td>use_bot</td>
						</tr>';   
		//add row in html-table for each record returned.
		while($row = $results->fetch_object()){    
        	$htmlstring.='<tr class="datarow"><td>'.$row->userID.'</td>';
			$htmlstring.='<td>'.$row->timestamp.'</td>';
			$htmlstring.='<td>'.$row->question1.'</td>';
			$htmlstring.='<td>'.$row->question2.'</td>';
			$htmlstring.='<td>'.$row->question3.'</td>';
			$htmlstring.='<td>'.$row->question4.'</td>';
			$htmlstring.='<td>'.$row->question5.'</td>';
			$htmlstring.='<td>'.$row->question6.'</td>';
			$htmlstring.='<td>'.$row->use_chatbot.'</td></tr>';
		}
		// after last datarow: close table
		$htmlstring.='</table>';

		// Publish question draft in temporary backend page as requested. makes no sense but doesnt't hurt i guess.
		$htmlstring.='<h2>Draft survey questions:</h2>';
		$strJsonFileContents = file_get_contents("questions.json");
		// Convert to array 
		$array = json_decode($strJsonFileContents, true);
		foreach ($array as $key => $value) {
			// $arr[3] will be updated with each value from $arr...
			$htmlstring.='<br>'.$key.' => '.$value;
		}
				
		// close doc
		$htmlstring.='</body></html>';
    return $htmlstring;
}
?>

