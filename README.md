# README #

Steps are necessary to get the application up and running:

1) copy files to the site root directory of a php(7.4)-enabled webserver. 
2) create db table using the SETUP/create-table.sql (current mysql/mariadb-version (tested on v10.4) is recommended. constraints don't work in mariadb <= v10.2)
3) enter your db credentials in SETUP/conf.json
4) create a chatbot agent in dialogflow *https://dialogflow.cloud.google.com/* and import the zip file in SETUP/ 
5) done... for testing you can open <yoursite>/databums.php in browser or start posting to it

## What is this repo for? ##

Objective is to to receive http-calls GET, GET(uid=x) or POST(string id, timestamp, int q1..q4) containing user responses to survey questions to mysql-database table "surveys".

Switch to simpler backend architecture for parent project. Old build (python/flask --> sqlite) is here: *https://bitbucket.org/chatbot-to-help/pythonapi*
