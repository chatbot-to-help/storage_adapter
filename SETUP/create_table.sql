--------------------------------------------------
-- db setup for chatbot backend ------------------
--------------------------------------------------


-- Create the database to hold the table(s). If a db by that name already exists, use it instead of overwriting.
CREATE DATABASE IF NOT EXISTS `chatbot`;

-- Create the table used for saving users' survey responses. If the db is non-empty and contains a table by that name, it will be overwritten.
CREATE OR REPLACE TABLE `chatbot`.`surveys` 
(   
	`userID` varchar(100) DEFAULT "unknown" NOT NULL,   
	`timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,   
	`question1` tinyint(1) NOT NULL, CHECK (question1 = 0 OR question1 = 1),   
	`question2` tinyint(1) NOT NULL, CHECK (question2 > 0 AND question2 < 8),   
	`question3` tinyint(1) NOT NULL, CHECK (question3 > 0 AND question3 < 8),   
	`question4` tinyint(1) NOT NULL, CHECK (question4 > 0 AND question4 < 8),   
	`question5` tinyint(1) NOT NULL, CHECK (question5 > 0 AND question5 < 8),   
	`question6` tinyint(1) NOT NULL, CHECK (question6 > 0 AND question6 < 8),   
	`use_chatbot` tinyint(1) NOT NULL, CHECK (use_chatbot = 0 OR use_chatbot = 1)
);
